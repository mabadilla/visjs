# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'visjs/version'

Gem::Specification.new do |spec|
  spec.name          = "visjs"
  spec.version       = Visjs::VERSION
  spec.authors       = ["MJ Abadilla"]
  spec.email         = ["mjmaix@gmail.com"]

  spec.summary       = %q{Gemify vis.js library for Rails assets pipeline}
  spec.description   = %q{Wrapping vis.js library with ruby gem for easy use in Rails projects}
  spec.homepage      = "https://github.com/mjmaix/visjs"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "https://rubygems.org"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.13"
  spec.add_development_dependency "rake", "~> 10.0"
end
