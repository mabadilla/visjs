# Visjs

Vis.js is a dynamic, browser based visualization library. The library is designed to be easy to use, handle large amounts of dynamic data, and enable manipulation of the data. The library consists of the following components:

    - DataSet and DataView. A flexible key/value based data set. Add, update, and remove items. Subscribe on changes in the data set. A DataSet can filter and order items, and convert fields of items.
    - DataView. A filtered and/or formatted view on a DataSet.
    - Graph2d. Plot data on a timeline with lines or barcharts.
    - Graph3d. Display data in a three dimensional graph.
    - Network. Display a network (force directed graph) with nodes and edges.
    - Timeline. Display different types of data on a timeline.

### The vis.js library was initialy developed by Almende B.V.

This project is wrapped as ruby gem.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'visjs'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install visjs

## Usage

Add this line to your application's assets/javascripts/application.js:

```ruby
//= require vis
```

and also, add this line to your application's assets/stylesheets/application.scss:

```css
...
@import "vis";
...
```

Finally, add this line to your applications' config/initializers/assets.rb:

```ruby
Rails.application.config.assets.precompile += %w( network/* )
```

If you want detailed information on it usage, you can refer to original documentation.

https://github.com/almende/vis

## Changelog

  - 0.1.0:     updated to vis.js 4.17.0

## Contributing

1. Fork it ( https://github.com/[my-github-username]/visjs/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
